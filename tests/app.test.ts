import * as request from 'supertest';
import { expect } from 'chai';

import * as http from 'http';

import App from '../src/app';

describe('GET /', () => {
  it('should return 200 OK', () => {
    const server = http.createServer(App);
    server.listen(process.env.PORT);

    return request(server)
      .get('/')
      .expect(200)
      .then(res => {
        expect(res.body).have.property('message');
        expect(res.body).have.property('version');
        server.close();
      });
  });

  it('should return 200 OK', () => {
    const server = http.createServer(App);
    server.listen(process.env.PORT);

    return request(server)
      .get('/Victor')
      .expect(200)
      .then(res => {
        expect(res.body).have.property('message');
        expect(res.body.message).contain('Victor');
        expect(res.body).have.property('version');
        server.close();
      });
  });

});
