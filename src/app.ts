import * as express from 'express';
import * as path from 'path';
import * as bodyParser from 'body-parser';
import * as helmet from 'helmet';
import * as pino from 'express-pino-logger';
const logger = pino();

class App {

  public express: express.Application;

  constructor() {
    this.express = express();
    this.middleware();
    this.routes();
  }

  private middleware = (): void => {
    this.express.use(helmet());
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({extended: false}));
    this.express.use(logger);
    this.express.use(express.static(path.join(__dirname, 'public')));
    this.express.use((req, res, next) => {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
      res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
      next();
    });
  }

  private routes = (): void => {

    this.express.get('/:name', (req, res, next) => {
      res.json({
        message: `Welcome to API skeleton, ${req.params.name}.`,
        version: '1.0.0'
      });
    });

    this.express.get('/', (req, res, next) => {
      res.json({
        message: `Welcome to API skeleton.`,
        version: '1.0.0'
      });
    });
  }

}

export default new App().express;
